#pragma once
#include <string>

class Customer
{
private:
	const std::string name;
	bool member = false;
	std::string memberType;
public:
	Customer(std::string);
	std::string getName();
	bool isMember();
	void setMember(const bool);
	std::string getMemberType();
	void setMemberType(const std::string);
	std::string toString();
	~Customer();
};

