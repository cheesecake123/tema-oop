#pragma once
#include <string>
class DiscountRate
{
private:
	static const double serviceDiscountPremium;
	static const double serviceDiscountGold;
	static const double serviceDiscountSilver;
	static const double productDiscountPremium;
	static const double productDiscountGold;
	static const double productDiscountSilver;
public:
	DiscountRate();
	static double getServiceDiscount(std::string);
	static double getProductDiscount(std::string);
	~DiscountRate();
};

