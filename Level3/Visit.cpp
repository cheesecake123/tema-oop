#include "Visit.h"

Visit::~Visit()
{
}

Visit::Visit(std::string name, Date date):customer(Customer(name)),date(date)
{	
}

std::string Visit::getName()
{
	return customer.getName();
}

double Visit::getServiceExpense()
{
	return serviceExpense;
}

void Visit::setServiceExpense(const double exp)
{
	serviceExpense = exp;
}

double Visit::getProductExpense()
{
	return productExpense;
}

void Visit::setProductExpense(const double exp)
{
	productExpense = exp;
}

double Visit::getTotalExpense()
{
	if (customer.isMember())
	{
		double servDisc = DiscountRate::getServiceDiscount(customer.getMemberType());
		double prodDisc = DiscountRate::getProductDiscount(customer.getMemberType());
		return (1 - servDisc)*serviceExpense + (1 - prodDisc)*productExpense;
	}
	return serviceExpense + productExpense;
}

std::string Visit::toString()
{
	std::string res="";
	char aux[20];
	res.append(customer.toString() + "\n");
	sprintf(aux, "%d|%d|%d\n", date.day, date.month, date.year);
	res.append(aux);
	sprintf(aux, "Cost: %0.2lf\n", getTotalExpense());
	res.append(aux);
	return res;
}
