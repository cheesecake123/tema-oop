#pragma once
#include "DiscountRate.h"
#include "Customer.h"
#include <string>
#include <chrono>

struct Date {
	const unsigned int day, month, year;
	Date(unsigned int d, unsigned int m, unsigned int y):day(d),month(m),year(y)
	{}
};

class Visit
{
private:
	const Date date;
	double serviceExpense;
	double productExpense;
public:
	Customer customer;

	Visit(std::string, Date);
	std::string getName();
	double getServiceExpense();
	void setServiceExpense(const double);
	double getProductExpense();
	void setProductExpense(const double);
	double getTotalExpense();
	std::string toString();
	~Visit();
};

