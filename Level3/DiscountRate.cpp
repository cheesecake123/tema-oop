#include "DiscountRate.h"

const double DiscountRate::serviceDiscountPremium = 0.2;
const double DiscountRate::serviceDiscountGold = 0.15;
const double DiscountRate::serviceDiscountSilver = 0.1;
const double DiscountRate::productDiscountPremium = 0.1;
const double DiscountRate::productDiscountGold = 0.1;
const double DiscountRate::productDiscountSilver = 0.1;

DiscountRate::DiscountRate()
{
}

double DiscountRate::getServiceDiscount(std::string type)
{
	if (type.compare("premium") == 0)
		return serviceDiscountPremium;
	if (type.compare("gold") == 0 )
		return serviceDiscountGold;
	return serviceDiscountSilver;
}

double DiscountRate::getProductDiscount(std::string type)
{
	if (type.compare("premium") == 0)
		return productDiscountPremium;
	if (type.compare("gold") == 0)
		return productDiscountGold;
	return productDiscountSilver;
}


DiscountRate::~DiscountRate()
{
}
