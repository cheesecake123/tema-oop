#include "Customer.h"

Customer::Customer(std::string name):name(name)
{

}
std::string Customer::getName()
{
	return name;
}
bool Customer::isMember()
{
	return member;
}
void Customer::setMember(const bool member)
{
	this->member = member;
}
std::string Customer::getMemberType()
{
	return memberType;
}
void Customer::setMemberType(const std::string type)
{
	this->memberType = type;
}
std::string Customer::toString()
{
	std::string res = "";
	res.append("Name: " + name + " ");
	if (!member)
	{
		res.append("- not member");
	}
	else
	{
		res.append("- " + memberType + " member");
	}
	return res;
}
Customer::~Customer()
{
}
