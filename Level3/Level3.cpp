#include <iostream>
#include <string>
#include "Visit.h"
#include "Customer.h"

using namespace std;

int main()
{
	Visit visita("Raul", Date(10, 04, 2017));
	visita.setProductExpense(100.0);
	visita.setServiceExpense(30.8);
	visita.customer.setMember(true);
	visita.customer.setMemberType("premium");
	cout << visita.toString()<<endl;
    return 0;
}

