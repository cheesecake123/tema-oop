#pragma once
#include <string>
#include <map>
#include <iostream>
#include "Money.h"
class CreditCard
{
private:
	std::string name;
	const std::string number;
	std::map<const std::string, const Money> transaction;
public:
	CreditCard(std::string, std::string);
	~CreditCard();
	void print();
	void chargeWithMoney(std::string, Money);
	void chargeWithNumbers(std::string, double, double);
};

