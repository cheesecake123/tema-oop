#include "Money.h"

Money::Money(int e, int c):euros(e), centimes(c)
{
	;
}

int Money::getEuros()
{
	return euros;
}

void Money::setEuros(const int e)
{
	this->euros = e;
}

int Money::getCentimes()
{
	return centimes;
}

void Money::setCentimes(const int c)
{
	centimes = c;
}

void Money::print() const
{
	printf("%d,%02d Euros", euros, centimes);
}

Money Money::operator+(const Money op)
{
	Money res = op;
	res.euros += this->euros;
	res.centimes += this->centimes;
	if (res.centimes > 99)
	{
		res.centimes -= 100;
		res.euros++;
	}
	return res;
}

Money Money::operator-(const Money op)
{
	Money res = *this;
	res.euros -= op.euros;
	res.centimes -= op.centimes;
	if (res.centimes < 0)
	{
		res.centimes += 100;
		res.euros--;
	}
	return res;
}

Money Money::operator*(const int factor)
{
	Money res = *this;
	res.euros *= factor;
	res.centimes *= factor;
	if (res.centimes > 99)
	{
		res.euros += res.centimes / 100;
		res.centimes %= 100;
	}
	return res;
}

Money Money::operator/(const int factor)
{
	Money res = *this;
	res.centimes += (res.euros % factor)*100;
	res.euros /= factor;
	res.centimes /= factor;
	return res;
}

bool Money::operator==(const Money op)
{
	return (this->euros==op.euros && this->centimes==op.centimes);
}

bool Money::operator!=(const Money op)
{
	return !(*this==op);
}

Money::~Money()
{
}
