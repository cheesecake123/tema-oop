#include <iostream>
#include "Money.h"
#include "CreditCard.h"

using namespace std;

int main()
{
	Money a(2, 99);
	(a + a + a).print();
	cout << endl;
	CreditCard cc("Raul","12avaf");
	CreditCard cc2 = cc;
	cc2.print();
	cout << endl;
    return 0;
}

