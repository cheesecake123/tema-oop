#pragma once
#include "cstdio"
class Money
{
private:
	int euros, centimes;
public:
	Money(int, int);
	int getEuros();
	void setEuros(const int);
	int getCentimes();
	void setCentimes(const int);
	void print() const;

	Money operator + (const Money);
	Money operator - (const Money);
	Money operator * (const int);
	Money operator / (const int);

	bool operator ==(const Money); 
	bool operator !=(const Money);
	~Money();
};

