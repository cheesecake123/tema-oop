#include "CreditCard.h"



CreditCard::CreditCard(std::string name, std::string number):name(name), number(number)
{
	;
}


CreditCard::~CreditCard()
{
}

void CreditCard::print()
{
	for (auto& t : transaction)
	{
		std::cout << t.first << " ";
		t.second.print();
		std::cout << std::endl;
	}
}

void CreditCard::chargeWithMoney(const std::string item, const Money cost)
{
	transaction.insert(std::pair<const std::string, const Money>(item, cost));
}

void CreditCard::chargeWithNumbers(const std::string item, double e, double c)
{
	transaction.insert(std::pair<const std::string, const Money>(item, Money(e,c)));
}
